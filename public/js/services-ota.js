document.getElementsByTagName("body")[0].classList.add("service-ota");
document.getElementById("block_khuhoi").classList.add("d-none");
function convertDateToString(date, count) {
  let _date = new Date(date.getTime());
  if (count > 0) {
    _date.setDate(_date.getDate() + count);
  }
  return (
    _date.getDate() + "/" + (_date.getMonth() + 1) + "/" + _date.getFullYear()
  );
}
var formOta = {
  flatpickrEnd: null,
  flatpickrBegin: null,
  isKhuHoi: false,
  getIsKhuHoi: () => formOta.isKhuHoi,
  setOtaDate: (fromDate, toDate, isKhuHoi) => {
    if (fromDate) {
      formOta.flatpickrBegin.setDate(fromDate);
    }
    if (toDate) {
      formOta.isKhuHoi = true;
    } else {
      formOta.isKhuHoi = false;
    }
    if (isKhuHoi !== null) {
      formOta.isKhuHoi = isKhuHoi;
    }
    updateDateByIsKhuHoi(fromDate, toDate);
    validIsKhuHoi();
  },
};
function updateDateByIsKhuHoi(fromDate, toDate) {
  if (formOta.isKhuHoi) {
    formOta.flatpickrEnd.setDate(toDate);
    formOta.flatpickrEnd.set("minDate", fromDate);
  } else {
    formOta.flatpickrEnd.setDate(null);
    formOta.flatpickrEnd.set("minDate", null);
  }
}
function validIsKhuHoi() {
  if (formOta.isKhuHoi) {
    $("#block_khuhoi").removeClass("d-none");

    $("#inputKhuHoiDesktop").prop("checked", true);
    $("#inputKhuHoiMobile").prop("checked", true);
  } else {
    $("#block_khuhoi").addClass("d-none");

    $("#inputKhuHoiDesktop").prop("checked", false);
    $("#inputKhuHoiMobile").prop("checked", false);
  }
}
function isFromDateBeforeToDate(fromDate, toDate) {
  if (fromDate.getFullYear() > toDate.getFullYear()) {
    return false;
  }
  if (fromDate.getMonth() > toDate.getMonth()) {
    return false;
  }
  if (fromDate.getDate() > toDate.getDate()) {
    return false;
  }
  return true;
}
$(() => {
  !(function (n, h) {
    "object" == typeof exports && "undefined" != typeof module
      ? h(exports)
      : "function" == typeof define && define.amd
      ? define(["exports"], h)
      : h(
          ((n =
            "undefined" != typeof globalThis ? globalThis : n || self).vn = {})
        );
  })(this, function (n) {
    "use strict";
    var h =
        "undefined" != typeof window && void 0 !== window.flatpickr
          ? window.flatpickr
          : {
              l10ns: {},
            },
      e = {
        weekdays: {
          shorthand: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
          longhand: [
            "Chủ nhật",
            "Thứ hai",
            "Thứ ba",
            "Thứ tư",
            "Thứ năm",
            "Thứ sáu",
            "Thứ bảy",
          ],
        },
        months: {
          shorthand: [
            "Th1",
            "Th2",
            "Th3",
            "Th4",
            "Th5",
            "Th6",
            "Th7",
            "Th8",
            "Th9",
            "Th10",
            "Th11",
            "Th12",
          ],
          longhand: [
            "Tháng một",
            "Tháng hai",
            "Tháng ba",
            "Tháng tư",
            "Tháng năm",
            "Tháng sáu",
            "Tháng bảy",
            "Tháng tám",
            "Tháng chín",
            "Tháng mười",
            "Tháng mười một",
            "Tháng mười hai",
          ],
        },
        firstDayOfWeek: 1,
        rangeSeparator: " đến ",
      };
    h.l10ns.vn = e;
    var T = h.l10ns;
    (n.Vietnamese = e),
      (n.default = T),
      Object.defineProperty(n, "__esModule", {
        value: !0,
      });
  });

  var nowDate = new Date();
  var minDateStr = convertDateToString(nowDate, 1);
  var endDateStr = convertDateToString(nowDate, 3);

  $("[date-input='end']").val(null);
  $("[date-input='begin']").val(minDateStr);

  formOta.flatpickrEnd = $("[date-input='end']").flatpickr({
    minDate: endDateStr,
    dateFormat: "d-m-Y",
    locale: "vn",
    position: "below",
    disableMobile: true,
    monthSelectorType: "static",
  });

  formOta.flatpickrBegin = $("[date-input='begin']").flatpickr({
    minDate: minDateStr,
    dateFormat: "d-m-Y",
    position: "below",
    disableMobile: true,
    locale: "vn",
    // inline: true,
    monthSelectorType: "static",
    onChange: function (selectedDates, dateStr, instance) {
      if (formOta.flatpickrEnd.selectedDates.length === 0) {
        formOta.flatpickrEnd.setDate(convertDateToString(selectedDates[0], 2));
        formOta.flatpickrEnd.set(
          "minDate",
          convertDateToString(selectedDates[0], 2)
        );
      } else {
        if (
          formOta.flatpickrEnd.selectedDates[0] &&
          !isFromDateBeforeToDate(
            selectedDates[0],
            formOta.flatpickrEnd.selectedDates[0]
          )
        ) {
          formOta.flatpickrEnd.setDate(
            convertDateToString(selectedDates[0], 2)
          );
          formOta.flatpickrEnd.set(
            "minDate",
            convertDateToString(selectedDates[0], 2)
          );
        }
      }
    },
  });
  formOta.flatpickrBegin.set("minDate", minDateStr);
  formOta.flatpickrEnd.set("minDate", endDateStr);

  $("#inputKhuHoiDesktop, #inputKhuHoiMobile").on("click", (e) => {
    formOta.isKhuHoi = $("#" + e.currentTarget.id).is(":checked")
      ? true
      : false;
    validIsKhuHoi();
    updateDateByIsKhuHoi(
      convertDateToString(formOta.flatpickrBegin.selectedDates[0], 0),
      convertDateToString(formOta.flatpickrBegin.selectedDates[0], 2)
    );
  });
});

$(document).ready(function () {
  //https://run.mocky.io/v3/cd464d52-38d6-4cf7-820c-52f23e21bde0
  // http://dev.momo.vn/__get/OTA/SearchLocation?t=1618479162445&Keywords=da&PageIndex=0&PageSize=10&ExcludeIds=6

  const Items = [
    {
      Id: 3,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "SGN",
      JsNa: "Tan Son Nhat International Airport",
      JsCi: "Ho Chi Minh",
      JsCo: "Vietnam",
      JsNv: "Sân bay Tân Sơn Nhất",
      JsCv: "Hồ Chí Minh",
      JsCov: "Việt Nam",
      JsCt: "VN",
      UrlRewrite: "ho-chi-minh",

      JsId: "SGN",
      JsNa: "Tan Son Nhat International Airport",
      JsCv: "Hồ Chí Minh",
      JsCi: "Ho Chi Minh",
      THEMMOI: "Hồ Chí Minh (SGN)",
    },
    {
      Id: 7,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "DAD",
      JsNa: "Da Nang Airport",
      JsCi: "Da Nang",
      JsCo: "Vietnam",
      JsNv: "Sân bay Đà Nẵng",
      JsCv: "Đà Nẵng",
      JsCov: "Việt Nam",
      JsCt: "VN",
      UrlRewrite: "da-nang",
    },
    {
      Id: 8,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "VDH",
      JsNa: "Dong Hoi Airport",
      JsCi: "Quang Binh",
      JsCo: "Vietnam",
      JsNv: "Sân bay Đồng Hới",
      JsCv: "Quảng Bình",
      JsCov: "Việt Nam",
      JsCt: "VN",
      UrlRewrite: "quang-binh",
    },
    {
      Id: 9,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "CXR",
      JsNa: "Cam Ranh International Airport",
      JsCi: "Nha Trang",
      JsCo: "Vietnam",
      JsNv: "Sân bay Cam Ranh",
      JsCv: "Nha Trang",
      JsCov: "Việt Nam",
      JsCt: "VN",
      UrlRewrite: "nha-trang",
    },
    {
      Id: 10,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "PQC",
      JsNa: "Phu Quoc International Airport",
      JsCi: "Phu Quoc",
      JsCo: "Vietnam",
      JsNv: "Sân bay Phú Quốc",
      JsCv: "Phú Quốc",
      JsCov: "Việt Nam",
      JsCt: "VN",
      UrlRewrite: "phu-quoc",
    },
    {
      Id: 11,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "DLI",
      JsNa: "Lien Khuong Airport",
      JsCi: "Da Lat",
      JsCo: "Vietnam",
      JsNv: "Sân bay Liên Khương",
      JsCv: "Đà Lạt",
      JsCov: "Việt Nam",
      JsCt: "VN",
      UrlRewrite: "da-lat",
    },
    {
      Id: 12,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "HUI",
      JsNa: "Phu Bai Airport",
      JsCi: "Hue",
      JsCo: "Vietnam",
      JsNv: "Sân bay Phú Bài",
      JsCv: "Huế",
      JsCov: "Việt Nam",
      JsCt: "VN",
      UrlRewrite: "hue",
    },
    {
      Id: 13,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "VII",
      JsNa: "Vinh International Airport",
      JsCi: "Vinh",
      JsCo: "Vietnam",
      JsNv: "Sân bay Vinh",
      JsCv: "Vinh",
      JsCov: "Việt Nam",
      JsCt: "VN",
      UrlRewrite: "vinh",
    },
    {
      Id: 14,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "TBB",
      JsNa: "Tuy Hoa Airport",
      JsCi: "Tuy Hoa",
      JsCo: "Vietnam",
      JsNv: "Sân bay Tuy Hòa",
      JsCv: "Tuy Hòa",
      JsCov: "Việt Nam",
      JsCt: "VN",
      UrlRewrite: "tuy-hoa",
    },
    {
      Id: 15,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "THD",
      JsNa: "Tho Xuan Airport",
      JsCi: "Thanh Hoa",
      JsCo: "Vietnam",
      JsNv: "Sân bay Thọ Xuân",
      JsCv: "Thanh Hóa",
      JsCov: "Việt Nam",
      JsCt: "VN",
      UrlRewrite: "thanh-hoa",
    },
    {
      Id: 118,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "CPC",
      JsNa: "Aviador C. Campos Airport",
      JsCi: "San Martin Des Andes",
      JsCo: "Argentina",
      JsNv: "Sân bay Aviador C. Campos",
      JsCv: "San Martin Des Andes",
      JsCov: "Argentina",
      JsCt: "AR",
      UrlRewrite: "san-martin-des-andes",
    },
    {
      Id: 119,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "VLG",
      JsNa: "Villa Gesell Airport",
      JsCi: "Villa Gesell",
      JsCo: "Argentina",
      JsNv: "Sân bay Villa Gesell",
      JsCv: "Villa Gesell",
      JsCov: "Argentina",
      JsCt: "AR",
      UrlRewrite: "villa-gesell",
    },
    {
      Id: 120,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "TDL",
      JsNa: "Héroes De Malvinas Airport",
      JsCi: "Tandil",
      JsCo: "Argentina",
      JsNv: "Sân bay Héroes De Malvinas",
      JsCv: "Tandil",
      JsCov: "Argentina",
      JsCt: "AR",
      UrlRewrite: "tandil",
    },
    {
      Id: 129,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "USH",
      JsNa: "Malvinas Argentinas Airport",
      JsCi: "Ushuaia",
      JsCo: "Argentina",
      JsNv: "Sân bay Malvinas Argentinas",
      JsCv: "Ushuaia",
      JsCov: "Argentina",
      JsCt: "AR",
      UrlRewrite: "ushuaia",
    },
    {
      Id: 130,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "RGL",
      JsNa: "Piloto Civil N. Fernández Airport",
      JsCi: "Rio Gallegos",
      JsCo: "Argentina",
      JsNv: "Sân bay Piloto Civil N. Fernández",
      JsCv: "Rio Gallegos",
      JsCov: "Argentina",
      JsCt: "AR",
      UrlRewrite: "rio-gallegos",
    },
    {
      Id: 134,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "VDM",
      JsNa: "Gobernador Castello Airport",
      JsCi: "Viedma",
      JsCo: "Argentina",
      JsNv: "Sân bay Gobernador Castello",
      JsCv: "Viedma",
      JsCov: "Argentina",
      JsCt: "AR",
      UrlRewrite: "viedma",
    },
    {
      Id: 137,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "CRD",
      JsNa: "General E. Mosconi Airport",
      JsCi: "Comodoro Rivadavia",
      JsCo: "Argentina",
      JsNv: "Sân bay General E. Mosconi",
      JsCv: "Comodoro Rivadavia",
      JsCov: "Argentina",
      JsCt: "AR",
      UrlRewrite: "comodoro-rivadavia",
    },
    {
      Id: 151,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "VDR",
      JsNa: "Villa Dolores Airport",
      JsCi: "Villa Dolores",
      JsCo: "Argentina",
      JsNv: "Sân bay Villa Dolores",
      JsCv: "Villa Dolores",
      JsCov: "Argentina",
      JsCt: "AR",
      UrlRewrite: "villa-dolores",
    },
    {
      Id: 156,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "SDE",
      JsNa: "Vicecomodoro Angel D. La Paz Aragonés Airport",
      JsCi: "Santiago Del Estero",
      JsCo: "Argentina",
      JsNv: "Sân bay Vicecomodoro Angel D. La Paz Aragonés",
      JsCv: "Santiago Del Estero",
      JsCov: "Argentina",
      JsCt: "AR",
      UrlRewrite: "santiago-del-estero",
    },
    {
      Id: 164,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "AEP",
      JsNa: "Jorge Newbery Airpark",
      JsCi: "Buenos Aires",
      JsCo: "Argentina",
      JsNv: "Jorge Newbery Airpark",
      JsCv: "Buenos Aires",
      JsCov: "Argentina",
      JsCt: "AR",
      UrlRewrite: "buenos-aires",
    },
    {
      Id: 262,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "NTL",
      JsNa: "Newcastle Airport",
      JsCi: "Newcastle",
      JsCo: "Australia",
      JsNv: "Sân bay Newcastle",
      JsCv: "Newcastle",
      JsCov: "Úc",
      JsCt: "AU",
      UrlRewrite: "newcastle",
    },
    {
      Id: 344,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "ZNE",
      JsNa: "Newman Airport",
      JsCi: "Newman",
      JsCo: "Australia",
      JsNv: "Sân bay Newman",
      JsCv: "Newman",
      JsCov: "Úc",
      JsCt: "AU",
      UrlRewrite: "newman",
    },
    {
      Id: 466,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "CAT",
      JsNa: "New Bight Airport",
      JsCi: "Cat Island",
      JsCo: "Bahamas",
      JsNv: "Sân bay New Bight",
      JsCv: "Cat Island",
      JsCov: "Bahamas",
      JsCt: "BS",
      UrlRewrite: "cat-island",
    },
    {
      Id: 2513,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "TNE",
      JsNa: "New Tanegashima Airport",
      JsCi: "Tanegashima",
      JsCo: "Japan",
      JsNv: "Sân bay New Tanegashima",
      JsCv: "Tanegashima",
      JsCov: "Nhật Bản",
      JsCt: "JP",
      UrlRewrite: "tanegashima",
    },
    {
      Id: 2579,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "CTS",
      JsNa: "New Chitose Airport",
      JsCi: "Sapporo",
      JsCo: "Japan",
      JsNv: "Sân bay New Chitose",
      JsCv: "Sapporo",
      JsCov: "Nhật Bản",
      JsCt: "JP",
      UrlRewrite: "sapporo",
    },
    {
      Id: 3051,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "NPL",
      JsNa: "New Plymouth Airport",
      JsCi: "New Plymouth",
      JsCo: "New Zealand",
      JsNv: "Sân bay New Plymouth",
      JsCv: "New Plymouth",
      JsCov: "New Zealand",
      JsCt: "NZ",
      UrlRewrite: "new-plymouth",
    },
    {
      Id: 3678,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "NCS",
      JsNa: "Newcastle Airport",
      JsCi: "Newcastle",
      JsCo: "South Africa",
      JsNv: "Sân bay Newcastle",
      JsCv: "Newcastle",
      JsCov: "Nam Phi",
      JsCt: "ZA",
      UrlRewrite: "newcastle",
    },
    {
      Id: 3802,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "PZU",
      JsNa: "Port Sudan New International Airport",
      JsCi: "Port Sudan",
      JsCo: "Sudan",
      JsNv: "Sân bay quốc tế Port Sudan New",
      JsCv: "Port Sudan",
      JsCov: "Sudan",
      JsCt: "SD",
      UrlRewrite: "port-sudan",
    },
    {
      Id: 3807,
      Avatar: null,
      JsonData: null,
      Content: null,
      JsId: "MWE",
      JsNa: "Merowe New Airport",
      JsCi: "Merowe",
      JsCo: "Sudan",
      JsNv: "Sân bay Merowe New",
      JsCv: "Merowe",
      JsCov: "Sudan",
      JsCt: "SD",
      UrlRewrite: "merowe",
    },
  ];

  // var listAirport = new Bloodhound({
  //   datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),
  //   queryTokenizer: Bloodhound.tokenizers.whitespace,
  //   // prefetch: Items,

  //   remote: {
  //     // url: 'https://run.mocky.io/v3/cd464d52-38d6-4cf7-820c-52f23e21bde0',
  //     url:
  //       "http://dev.momo.vn/__get/OTA/SearchLocation?t=1618479162445&Keywords=%QUERY&PageIndex=0&PageSize=10&ExcludeIds=6",
  //     //   wildcard: "%QUERY",

  //     prepare: function (query, settings) {
  //       // settings.url = settings.url + '&q=' + query;
  //       settings.url =
  //         "http://dev.momo.vn/__get/OTA/SearchLocation?t=1618479162445&Keywords=" +
  //         query +
  //         "&PageIndex=0&PageSize=10&ExcludeIds=6";
  //       settings.headers = {
  //         "X-Requested-With": "XMLHttpRequest",
  //       };
  //       return settings;
  //     },
  //     filter: function (list) {
  //       // console.log(list.Items);
  //       return list.Items;
  //       // return $.map(list, function (name) {
  //       //   return { user: name };
  //       // });
  //     },
  //   },
  //   identify: function (obj) {
  //     console.log.og(obj);
  //     return obj.Items;
  //   },
  // });

  // $("#the-start .typeahead").typeahead(
  //   {
  //     hint: true,
  //     highlight: true,
  //     minLength: 0,
  //   },
  //   {
  //     limit: 8,
  //     name: "JsCi",
  //     display: Handlebars.compile("{{JsCi}} – ({{JsId}})"),
  //     source: listAirport,
  //     templates: {
  //       empty: [
  //         '<div class="empty-message">',
  //         "Không tìm thấy kết quả",
  //         "</div>",
  //       ].join("\n"),
  //       suggestion: Handlebars.compile(
  //         "<div class='tt-suggest-item'><div class='font-weight-bold'>{{JsCv}}</div><div class='text-small opacity-60 mt-1 '>{{JsId}} – {{JsNv}}</div></div>"
  //       ),
  //     },
  //   }
  // );

  // $(".typeahead").bind("typeahead:select", function (ev, suggestion) {
  //   $(".typeahead").typeahead("close");
  //   $(".typeahead").blur();
  // });

  //the-start-btn
  //the-start-btn

  // const theStartBtn = $("#the-start-btn");
  // const theStartBox = $("#the-start");

  // theStartBtn.click(function () {
  //   $(this).addClass("ota-box-show");
  //   setTimeout(function () {
  //     theStartBox.addClass("is-active");
  //   }, 200);
  // });

  // $(".ota-box-function").each(function () {
  //   const theBtn = $(this).find(".ota-box-maker");
  //   const theBox = $(this);
  //   const theBoxFocus = $(this).find(".ota-box-search .form-control");

  //   theBtn.click(function () {
  //     $(this).addClass("ota-box-show");
  //     setTimeout(function () {
  //       theBox.addClass("is-active");
  //       theBoxFocus.focus()
  //     }, 200);
  //   });

  // });

  // const popperInstance = Popper.createPopper(theStartBtn, theStartBox);
  // theStartBox.hide();

  // var popper = new Popper(theStartBtn, theStartBox, {
  //   placement: "bottom-start",
  //   onCreate: function (data) {
  //     console.log(data);
  //   },
  //   modifiers: {
  //     flip: {
  //       behavior: ["left", "right", "top", "bottom"],
  //     },
  //     offset: {
  //       enabled: true,
  //       offset: "0",
  //     },
  //   },
  // });

  $("#dropdown-noidi").on("show.bs.dropdown", function () {
    const theBoxFocus = $(this).find(".ota-box-search .form-control");
    setTimeout(function () {
      theBoxFocus.focus();
    }, 200);
  });

  $("#dropdown-noiden").on("show.bs.dropdown", function () {
    const theBoxFocus = $(this).find(".ota-box-search .form-control");
    setTimeout(function () {
      theBoxFocus.focus();
    }, 200);
  });

  $('.ota-box-popper').on('click', function(event){
    // The event won't be propagated up to the document NODE and 
    // therefore delegated events won't be fired
    event.stopPropagation();
});


});


// $('#ota-error-diadiem').popover('show');
// setTimeout(function(){
//   $('#ota-error-diadiem').popover('hide');
// }, 5000)