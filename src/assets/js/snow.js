const canvas = document.querySelector('.snow');
const ctx = canvas.getContext('2d');
const elSnow = $('canvas.snow');
let windowW = elSnow.width();// window.innerWidth,
let windowH = elSnow.height();// document.body.scrollHeight, //window.innerHeight,
const numFlakes = 200;
let flakes = [];
let snowReq;

const requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame
                            || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

const cancelAnimationFrame = window.cancelAnimationFrame || window.mozCancelAnimationFrame;


function randomBetween(min, max, round) {
  const num = Math.random() * (max - min + 1) + min;

  if (round) {
    return Math.floor(num);
  }
  return num;
}

function distanceBetween(vector1, vector2) {
  const dx = vector2.x - vector1.x;
  const dy = vector2.y - vector1.y;

  return Math.sqrt(dx * dx + dy * dy);
}

function newFlake(x, y) {
  const maxWeight = 3;
  const maxSpeed = 1;

  this.x = x;
  this.y = y;
  this.r = randomBetween(0, 1);
  this.a = randomBetween(0, Math.PI);
  this.aStep = 0.01;


  this.weight = randomBetween(2, maxWeight);
  this.alpha = (this.weight / maxWeight);
  this.speed = (this.weight / maxWeight) * maxSpeed;

  this.update = () => {
    this.x += Math.cos(this.a) * this.r;
    this.a += this.aStep;

    this.y += this.speed;
  };
}

function initMaxFlakesByDocumentWidth() {
  const dWidth = $(document).width();

  if (dWidth < 768) {
    return numFlakes / 2;
  }
  return numFlakes;
}

function initMaxFlakesByDocumentHeight() {
  const dHeight = $(document).height();
  const maxFlakes = initMaxFlakesByDocumentWidth();
  if (dHeight < 1000) {
    return maxFlakes / 4;
  }
  if (dHeight < 2000) {
    // eslint-disable-next-line radix
    return parseInt(maxFlakes / 3);
  }
  if (dHeight < 3000) {
    return maxFlakes / 2;
  }
  if (dHeight < 4000) {
    // eslint-disable-next-line radix
    return parseInt(maxFlakes - (maxFlakes / 4));
  }
  return maxFlakes;
}

function checkDocumentSize() {
  if (windowW !== elSnow.width()) {
    return true;
  }
  if (windowH !== elSnow.height()) {
    return true;
  }
  return false;
}

function clearCanvas() {
  // clear canvas
  ctx.save();
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  ctx.clearRect(0, 0, windowW, windowH);
  ctx.restore();
}

function loop() {
  if (checkDocumentSize()) {
    initSnow();
    return;
  }
  let i = flakes.length;
  let z;
  let dist;
  let flakeA;
  let flakeB;

  clearCanvas();

  // loop of hell
  // eslint-disable-next-line no-plusplus
  while (i--) {
    flakeA = flakes[i];
    flakeA.update();

    ctx.beginPath();
    ctx.arc(flakeA.x, flakeA.y, flakeA.weight, 0, 2 * Math.PI);
    ctx.shadowBlur = 6;
    ctx.shadowColor = 'rgba(0, 0, 0, 0.2)';
    ctx.closePath();
    ctx.save();
    ctx.fillStyle = `rgba(255, 255, 255, ${flakeA.alpha})`;
    ctx.fill();
    ctx.restore();

    if (flakeA.y >= windowH) {
      flakeA.y = -flakeA.weight;
    }
  }

  snowReq = requestAnimationFrame(loop);
}


function initSnow() {
  windowW = elSnow.width();// window.innerWidth,
  windowH = elSnow.height();// document.body.scrollHeight, //window.innerHeight,
  canvas.width = windowW;
  canvas.height = windowH;

  flakes = [];

  let i = initMaxFlakesByDocumentHeight();
  let flake;
  let x;
  let y;

  // eslint-disable-next-line no-plusplus
  while (i--) {
    x = randomBetween(0, windowW, true);
    y = randomBetween(0, windowH, true);

    flake = new newFlake(x, y);
    flakes.push(flake);
  }
  if (snowReq) {
    cancelAnimationFrame(snowReq);
    clearCanvas();
  }

  loop();
}

$(() => {
  initSnow();
});
window.onresize = () => {
  initSnow();
};
