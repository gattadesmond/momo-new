// const { now } = require("core-js/fn/date");

var $html = jQuery("html");

//Test Device
window.isMobile = false;
if (
  /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
    navigator.userAgent
  )
) {
  window.isMobile = true;
}

//Detect Mobile
if (window.isMobile) {
  $html.addClass("mobile");
} else {
  $html.addClass("desktop");
}

//Detect Browser
window.isFirefox = navigator.userAgent.toLowerCase().indexOf("firefox") > -1;
window.isSafari = /^((?!chrome).)*safari/i.test(navigator.userAgent);
window.isChrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase());
window.isChromeiOS = navigator.userAgent.match("CriOS");
window.isMSIE = navigator.userAgent.match("MSIE");
window.isAndroid = navigator.userAgent.toLowerCase().indexOf("android") > -1;
window.isiPad = navigator.userAgent.match(/iPad/i) !== null;

//Detect OS
window.isWindows = navigator.platform.toUpperCase().indexOf("WIN") !== -1;
window.isOSX = navigator.platform.toUpperCase().indexOf("MAC") !== -1;
window.isLinux = navigator.platform.toUpperCase().indexOf("LINUX") !== -1;

//Prepare for CSS Fixes
if (window.isSafari) {
  $html.addClass("safari");
}
if (window.isFirefox) {
  $html.addClass("firefox");
}
if (window.isChrome) {
  $html.addClass("chrome");
}
if (window.isMSIE) {
  $html.addClass("msie");
}
if (window.isAndroid) {
  $html.addClass("android");
}
if (window.isWindows) {
  $html.addClass("windows");
}
if (window.isOSX) {
  $html.addClass("osx");
}
if (window.isLinux) {
  $html.addClass("linux");
}

//Retina
window.isRetina =
  (window.matchMedia &&
    (window.matchMedia(
      "only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)"
    ).matches ||
      window.matchMedia(
        "only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)"
      ).matches)) ||
  (window.devicePixelRatio && window.devicePixelRatio > 1.3);

if (window.isRetina) {
  $html.addClass("retina");
}
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
var vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty("--vh", vh + "px");

// We listen to the resize event
window.addEventListener("resize", function () {
  // We execute the same script as before
  // let vh = window.innerHeight * 0.01;
  // document.documentElement.style.setProperty("--vh", `${vh}px`);
});

$.fn.hasVerticalScrollBar = function () {
  return this[0].clientHeight < this[0].scrollHeight;
};

$.fn.hasHorizontalScrollBar = function () {
  return this[0].clientWidth < this[0].scrollWidth;
};

function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

//Soju animation reveal like Sketch website
//Reveal class

if (!!window.IntersectionObserver) {
  let observer = new IntersectionObserver(
    (entries, item) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          entry.target.classList.add("show");
        } else {
          entry.target.classList.contains("reveal-auto-hide") &&
            entry.target.classList.remove("show");
        }
      });
    },
    { threshold: 0 }
  );

  document.querySelectorAll(".reveal").forEach((item) => {
    setTimeout(function () {
      observer ? observer.observe(item) : item.classList.add("show");
    }, Number(item.dataset.revealDelay) || 300);
  });
}
// All function
var context = {};
function detectDeviceType() {
  return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
    navigator.userAgent
  )
    ? "Mobile"
    : "Desktop";
};

function copyCode(element) {
  /* Select the text field */
  element[0].select();
  element[0].setSelectionRange(0, 99999); /*For mobile devices*/

  /* Copy the text inside the text field */
  return document.execCommand('copy');
}

function wrapScrollClass(wrap) {
  wrapScrollClassInit(wrap);
  window.addEventListener('resize', function () {
    wrapScrollClassInit(wrap);
  });
}

function wrapScrollClassInit(wrap) {
  var $wrap = wrap;
  var $tar = $wrap.find(".wrap__scroll__target ");
  var $btn = $wrap.find(".wrap__scroll__next");

  if ($wrap.hasHorizontalScrollBar()) {
    $wrap.addClass("wrap-active");

    // $wrap.each((index, element) => {
    //   const $element = $(element);

    //   const $section = $element.parents('section');
    //   $section.css('--wrap-bg-color', $section.css('background-color'));

    //   console.log($section.css('background-color'));
    // });


    $btn.on("click", function () {
      $tar.animate({ scrollLeft: $tar.scrollLeft() + 100 }, "fast");
    });
  } else {
    $wrap.removeClass("wrap-active");
    $btn.unbind("click");
  }
}

function BankSelectActive(target) {
  var itemId = $(target).data("item-id");
  var groupId = $(target).data("group-id");
  var eleGroupId = "#hd-" + groupId;
  var eleItemImageId = "#hd-sub-img-" + itemId;
  var eleItemProcessId = "#hd-sub-ctn-" + itemId;
  var bankSelectDevice = $(eleGroupId).find(".manual-device");
  var bankSelectDeviceStep = $(eleGroupId).find(".manual-process");

  bankSelectDevice.each(function (i, v) {
    $(this).removeClass("d-block").addClass("d-none");
  });
  $(eleItemImageId).removeClass("d-none").addClass("d-block");

  bankSelectDeviceStep.each(function (i, v) {
    $(this).removeClass("d-block").addClass("d-none");
  });
  $(eleItemProcessId).removeClass("d-none").addClass("d-block");
}

function mobileAndTabletCheck() {
  let check = false;
  (function (a) {
    if (
      /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(
        a
      ) ||
      /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
        a.substr(0, 4)
      )
    )
      check = true;
  })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
};

function execFunction(functionnName, ctx = context /*, args */) {
  try {
    // get passed arguments except first two (fnName, ctx)
    var args = Array.prototype.slice.call(arguments, 2);
    // execute the function with passed parameters and return result
    ctx[functionnName].apply(ctx, args);
  } catch (error) {
    console.error(error);
  }
}

function renderSVGInject() {
  SVGInjector && typeof SVGInjector === 'function' && SVGInjector(document.querySelectorAll('[data-inject-svg]'));
}






function initLoadAjax() {
  const divAjax = $('[data-loading]');
  if (divAjax.length) {
    divAjax.each(function(index, element) {
      const jqueryElement = $(element);
      const loadType = jqueryElement.attr('data-loading');
      const loadUrl = jqueryElement.attr('data-url');
      const loadHeader = jqueryElement.attr('data-header');
      const loadFunctionCallBack = jqueryElement.attr('data-js-init');

      if (loadType === 'ajax' && loadUrl) {
        $.get(loadUrl, loadHeader ? JSON.parse(loadHeader) : {})
          .done(function(response) {
            if (response && response.Data && response.Data.Html) {
              jqueryElement.append(response.Data.Html);
              renderSVGInject();
              if (loadFunctionCallBack) {
                execFunction(loadFunctionCallBack);
              };
            }
          });
      }
    });
  }
}

function intitDataCopy() {
  const dataCopy = $('[data-copy]');
  if (dataCopy.length) {
    dataCopy.on('click', function () {
      const $this = $(this);
      const element = $this.find('input');
      if (element.length > 0) {
        copyCode(element);
      }
    });
  }
}

function initHuongDan() {
  $(".manual-device").length && $(".manual-device").each(function (index, element) {
    var $this = $(this);
    var numSlider = $this.attr("childId");
    var device = null;
    var process = null;

    if (numSlider != undefined) {
      device = $this.find(".manual-device-swiper#hd-sub-swp-" + numSlider);
      process = $this
        .parent()
        .parent()
        .find(".manual-process#hd-sub-ctn-" + numSlider);
    } else {
      numSlider = index;
      device = $this.find(".manual-device-swiper");

      process = $this.parent().parent().find(".manual-process ");
    }

    var swiperArray = [];

    swiperArray[numSlider] = new Swiper(device, {
      observer: true,
      observeParents: true,
      spaceBetween: 0,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      on: {
        slideChangeTransitionEnd: function slideChangeTransitionEnd() {
          var num = this.activeIndex;
          process.children(".process_item").removeClass("active");
          process.children(".process_item").eq(num).addClass("active");
        },
      },
    });

    process.children(".process_item").each(function (index, element) {
      var num = index;
      $(element).on("click", function (e) {
        e.preventDefault();
        $(this).addClass("active").siblings().removeClass("active");
        swiperArray[numSlider].slideTo(index);
      });
    });
  });

  $(".process__body-content").length && $(".process__body-content").each(function (index, element) {
    var $this = $(this);
    var height = $this[0].scrollHeight;
    var getHeight = height == "0" ? "auto" : height + "px";
    $this.css("--max-height", getHeight);
  });

  //Chon ngan hang
  var bankSelectContainer = $(".manual-wrapepr .bank-dropdown");
  if (bankSelectContainer.length) {
    var bankSelectText = bankSelectContainer.find(".content");
    var bankSelectItem = bankSelectContainer.find(
      ".dropdown-menu .bank-dropdown-item"
    );
  
    //var bankSelectDevice = $(".manual-device-swiper-container").find(".manual-device");
    //var bankSelectDeviceStep = $(".manual-device-swiper-container").find(".manual-process");
    if (bankSelectItem.length) {
      bankSelectItem.each(function (i, v) {
        $(this).on("click", function (e) {
          BankSelectActive(this);
          e.preventDefault();
          bankSelectText.html($(this).html());
        });
      });
    }
  }

  // End chon ngan hang

  //Huong dan mobile
  if ($(".htu-slider").length) {
    var htuSwiper = new Swiper(".htu-slider", {
      loop: false,
      // Disable preloading of all images
      preloadImages: false,
      // Enable lazy loading
      lazy: {
        loadPrevNext: true,
      },
      slidesPerView: 1,

      effect: "fade",
      fadeEffect: {
        crossFade: true,
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
    });
  }
  
  $(".htu-modal").on("shown.bs.modal", function () {
    var swc = this.querySelector(".swiper-container");
    if (swc) {
      var mySwiper = swc.swiper;
      mySwiper.update();
      mySwiper.slideTo(0);
    }
  });
}

// End All function
// Khai báo để có thể sử dụng execFunction trong trường hợp Div load html bằng ajax và cần function callback

// context["intitDataCopy"] = intitDataCopy;
// context["initHuongDan"] = initHuongDan;

// Document ready - onload
$(() => {
  if (typeof AOS !== 'undefined' && AOS) {
    AOS && AOS.init({once:true});
  }
  renderSVGInject();

  //App download
  var appDownload = $(".app-download");
  if (appDownload.length) {
    $(window).on("scroll", function () {
      if ($(window).scrollTop() > 300) {
        appDownload.addClass("is-active");
      } else {
        appDownload.removeClass("is-active");
      }
    });
  }

  const stickySideBar = $(".sticky-sidebar");
  if (stickySideBar.length) {
    stickySideBar.theiaStickySidebar({
      // Settings
      additionalMarginTop: 60 + (stickySideBar.attr('additionalMarginTop') || 0),
    });
  }

  const kvSVG = $(".kv-svg");
  if (kvSVG.length) {
    kvSVG.rating({
      theme: "krajee-svg",
      filledStar: '<span class="krajee-icon krajee-icon-star"></span>',
      emptyStar: '<span class="krajee-icon krajee-icon-star"></span>',
      showCaption: false,
      starCaptions: {
        0: "Not Rated",
        1: "Very Poor",
        2: "Poor",
        3: "Ok",
        4: "Good",
        5: "Very Good",
      },
      starCaptionClasses: {
        0: "text-danger",
        1: "text-danger",
        2: "text-warning",
        3: "text-info",
        4: "text-primary",
        5: "text-success",
      },
    });
  }

  var menuItem = $(".nav-item.has-dropdown");
  var menuTimeout;
  menuItem.hover(
    function () {
      var $this = $(this);
      menuTimeout = setTimeout(function () {
        $this.addClass("is-active");
      }, 100);
    },
    function () {
      clearTimeout(menuTimeout);
      $(this).removeClass("is-active");
    }
  );

  //Swiper QR CODE
  // $("#qrcode-modal").on("shown.bs.modal", function (e) {
  //   var swiperQrcode = new Swiper(".swiper-qrcode", {
  //     pagination: {
  //       el: ".swiper-pagination",
  //     },
  //   });
  // });

  //Swiper KV
  const swiperKV = $(".swiper-kv");
  if (swiperKV.length) {
    if (swiperKV.find(".swiper-slide").length > 1) {
      new Swiper(".swiper-kv", {
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
        autoplay: {
          delay: 5000,
          disableOnInteraction: false,
        },
        speed: 800,
        preloadImages: false,
        lazy: true
      });
    } else {
      swiperKV.find(".kv-pagination").hide();
    }
  }

  //Swiper KV Promotion
  const swiperKvPromotion = $(".swiper-kv-promotion");
  if (swiperKvPromotion.length > 0) {
    if (swiperKvPromotion.find(".swiper-slide").length > 1) {
      new Swiper(".swiper-kv-promotion", {
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
        autoplay: {
          delay: 5000,
          disableOnInteraction: false,
        },
        speed: 800,
      });
    } else {
      swiperKvPromotion.find(".kv-pagination").hide();
    }
  }

  feather.replace();

  // mobile-menu__btn
  var menuMobileBtn = document.querySelector("#mobile-menu__btn");
  var menuMobileElement = document.querySelector(".mobile-menu__wrap");

  menuMobileBtn &&
    menuMobileBtn.addEventListener("click", function (e) {
      e.preventDefault();

      if (menuMobileElement.classList.contains("is-open")) {
        menuMobileElement.classList.remove("is-open");

        menuMobileBtn.classList.remove("is-active");
        document.body.classList.remove("menu-open");
        // menuMobileElement.style.display = "none";
      } else {
        document.body.classList.add("menu-open");
        // menuMobileElement.style.display = "block";
        menuMobileElement.classList.add("is-open");
        menuMobileBtn.classList.add("is-active");
      }
    });

  $(".app-download__close").on("click", function () {
    $(".app-download").addClass("d-none");
  });

  // $(".wrap__scroll").length &&
  //   $(".wrap__scroll").each(function (e) {
  //     var $this = $(this);
  //     var $tar = $this.find(".wrap__scroll__target ");
  //     var $btn = $this.find(".wrap__scroll__next");

  //     if ($this.hasHorizontalScrollBar()) {
  //       $this.addClass("is-active");

  //       $btn.on("click", function () {
  //         console.log("dwdw");
  //         $tar.animate({ scrollLeft: $tar.scrollLeft() + 100 }, "fast");
  //       });
  //     }
  //   });

  $(".wrap__scroll__tintuc").length &&
    wrapScrollClass($(".wrap__scroll__tintuc"));

  // $(".tienich__cat__link").on("shown.bs.tab", function (e) {
  //   // e.target // newly activated tab
  //   // e.relatedTarget // previous active tab
  //   // console.log(e );
  // });

  intitDataCopy();

  // $(".next-nav").length &&
  //   $(".next-nav").on("click", function () {
  //     $(".wrap-scroll-nav").animate(
  //       { scrollLeft: $(".wrap-scroll-nav").scrollLeft() + 100 },
  //       "fast"
  //     );
  //   });
  // var e = setInterval(() => {
  //   $(".nav_cloned .next-nav").length &&
  //     ($(".nav_cloned .next-nav").on("click", function () {
  //       $(".nav_cloned .wrap-scroll-nav").animate(
  //         {
  //           scrollLeft: $(".nav_cloned .wrap-scroll-nav").scrollLeft() + 100,
  //         },
  //         "fast"
  //       );
  //     }),
  //     clearInterval(e));
  // });

  if ($(".swiper-new-col4").length) {
    new Swiper(".swiper-new-col4", {
      slidesPerView: 4,
      spaceBetween: 20,
      // init: false,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      breakpoints: {
        500: {
          slidesPerView: 1,
          spaceBetween: 20,
        },

        768: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 20,
        },
      },
    });
  }

  // Caching some stuff..
  const body = document.body;
  const docEl = document.documentElement;

  initHuongDan();

  $(".navbar-lp").length && $(".navbar-lp").on("click", function () {
    $(".navbar-collapse").collapse("hide");
  });

  
  $('[data-toggle="popover"]').length && $('[data-toggle="popover"]').popover();
  $(".process__body-content a").on("click", function (e) {
    window.open(this.href, "_blank");
  });

  //Feedback form

  $(".feedback__btn").on("click", function (e) {
    e.preventDefault();
    $(".feeback__wrapper").addClass("is-active");
  });

  $(".feedback__close").on("click", function (e) {
    e.preventDefault();
    $(".feeback__wrapper").removeClass("is-active is-input");
    $(".feedback__rating__item").removeClass("is-active ");
  });

  $(".feedback__rating .feedback__rating__item").on("click", function (e) {
    e.preventDefault();

    $(".feeback__wrapper").addClass("is-input");

    $(this).siblings().removeClass("is-active");
    $(this).addClass("is-active");
  });

  //Open popup nhận thông báo
  // $('#modalSlideUp').modal('show');

  // load Ajax
  initLoadAjax();

  new SmoothScroll('a[data-smooth-scroll]',
    {
      speedAsDuration: true,
      offset: $('body').attr('data-smooth-scroll-offset')
        || 0,
    });
});

// for footer
$(() => {
  $(".wrap__scroll__service__footer").length &&
    wrapScrollClass($(".wrap__scroll__service__footer"));
});
