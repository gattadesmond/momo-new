$(document).ready(function () {
  var swiperCineCast = new Swiper(".swiper-cine-cast", {
    slidesPerView: 5,
    spaceBetween: 10,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    breakpoints: {
      767: {
        slidesPerView: "auto",
        spaceBetween: 10,
      },
    },
  });

  var swiperCineGalley = new Swiper(".swiper-cine-gallery", {
    slidesPerView: "auto",
    spaceBetween: 5,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });

  var swiperPosterGallery = new Swiper(".swiper-poster-gallery", {
    slidesPerView: "auto",
    spaceBetween: 10,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });

  $(".cinema-loadmore").readmore({
    speed: 75,
    moreLink:
      '<a href="#">Xem thêm <img src="img/icons/interface/icon-chevron-down.svg" > </a>',
    lessLink:
      '<a href="#">Thu gọn  <img src="img/icons/interface/icon-chevron-up.svg" ></a>',
  });

  // var swiperBookingContainer=$(".cine__booking__swiper");

  // var swiperBooking = new Swiper(".cine__booking__swiper .swiper-container", {
  //   navigation: {
  //     nextEl: ".booking__swiper__navi.right",
  //     prevEl: ".booking__swiper__navi.left",
  //   },
  //   // slidesPerGroup: 2,
  //   spaceBetween: 8,
  //   slidesPerView: "auto",
  // });

  $(".movie__list__swiper").each(function(i, val){
      var $this = $(this);
      var swiperElement = $(this).find(".swiper-movie-list");

      var naviNext = $(this).find(".swiper-button-next");

      var naviPrev = $(this).find(".swiper-button-prev");

      var SwiperRun = new Swiper(swiperElement, {
        slidesPerView: "auto",
        spaceBetween: 20,
        navigation: {
          nextEl: naviNext,
          prevEl: naviPrev,
        },
      });
  })
  // var swiperMovieList = new Swiper(".swiper-movie-list", {
  //   slidesPerView: 5,
  //   spaceBetween: 25,
  //   navigation: {
  //     nextEl: ".movie__list__swiper .swiper-button-next",
  //     prevEl: ".movie__list__swiper .swiper-button-prev",
  //   },
  // });

});




function drawCanvasRating(canvasName) {
  let canvas = canvasName;
  let context = canvas.getContext("2d");

  // vi du 20%
  let percent = 80;
  let ratio = percent / 100 + 1;

  let my_gradient = context.createLinearGradient(0, 0, 170, 0);

  if (percent <= 75) {
    my_gradient.addColorStop(0, "#db2474");
    my_gradient.addColorStop(1, "#ff75b1");
  } else {
    my_gradient.addColorStop(0, "#0098f1");
    my_gradient.addColorStop(1, "#2f54eb");
  }

  context.beginPath();
  context.arc(90, 90, 85, Math.PI, 0 + Math.PI * ratio, false);
  context.lineCap = "round";
  context.lineWidth = 10;

  context.strokeStyle = my_gradient;

  context.fillStyle = "white";
  context.fill();
  // context.strokeStyle = "#6642fb";
  context.stroke();
}

document.querySelectorAll(".canvas-rating").forEach((item) => {
  drawCanvasRating(item);
});


