document.body.classList.add("header-relative");

// $('body').scrollspy({ target: '#post__spy', offset : 90 })




$(window).on("activate.bs.scrollspy", function (i) {
  // do something...
  // console.log("fewew");
  var text = $("#post__spy__mobile .active").text();
  // console.log(text);
  $(".post__spy__mb__btn span").html(text);

});

//BLOG JS
$(window).on("ready load resizeEnd", function () {
  var $articleContent = $(".post__wrap");
  if($articleContent.length <=0) return;
  var panelHeight = 55;
  var articleOffset = $articleContent.offset();

  //blog Header height
  window.headerHeight =  40;

  window.articleHeight = $articleContent.height();
  window.windowHeight = $(window).height();
  window.articleOffsetTop = articleOffset.top - panelHeight;
});


//ON SCROLL
$(window).on("scroll ready load resize resizeEnd", function (event) {
  // if (window.isMobile) return;

  var $body = $("body");
  var scrollTop = $(window).scrollTop();
  //   translateBackground = scrollTop / 3,
  //   scale = scrollTop / window.headerHeight,
  //   backgroundScale = 1, // + scale / 10
    scrollProgress =
      (scrollTop - window.articleOffsetTop) /
      (window.articleHeight - window.windowHeight / 6);

  // HEADER ANIMATION
  if (scrollTop < window.headerHeight) {


    // $(".post__feature__background .inner").css({
    //   "-webkit-transform":
    //     "scale(" +
    //     backgroundScale +
    //     ") translateY(" +
    //     translateBackground +
    //     "px)",
    //   transform:
    //     "scale(" +
    //     backgroundScale +
    //     ") translateY(" +
    //     translateBackground +
    //     "px)",
    //   opacity: 1 - scale / 1.2,
    // });

    $body.removeClass("contentPart");
  } else if (scrollTop >= window.headerHeight) {
    $body.addClass("contentPart");
  }

  if (scrollProgress >= 0 && scrollProgress <= 1) {
    $(".mgzs__panel .progress")
      .removeClass("hide")
      // .css("width", 100 * scrollProgress + "%");
      .attr("value", 100 * scrollProgress);
  } else if (scrollProgress > 1) {
    $body.removeClass("contentPart"); //.addClass('bottomPart');
  } else if (scrollProgress < 0) {
    $(".mgzs__panel .progress").addClass("hide");
  }
});

//END BLOG JS

// $(window).on("scroll ready load resize resizeEnd", function (event) {
//   if (window.isMobile) return;

//   var scrollTop = $(window).scrollTop(),
//     translateTitle = scrollTop / 2,
//     translateBackground = scrollTop / 3,
//     scale = scrollTop / window.headerHeight,
//     backgroundScale = 1, // + scale / 10
//     titleScale = 1 - scale * 0.1,
//     titleOpacity = 1 - scale,
//     scrollProgress =
//       (scrollTop - window.articleOffsetTop) /
//       (window.articleHeight - window.windowHeight / 6);

//   // HEADER ANIMATION
//   if (scrollTop < window.headerHeight) {
//     //shift the title
//     $("section.header .title-container").css({
//       "-webkit-transform":
//         "translateY(" + translateTitle + "px) scale(" + titleScale + ")",
//       transform:
//         "translateY(" + translateTitle + "px) scale(" + titleScale + ")",
//       opacity: titleOpacity,
//     });

//     //translate and scale
//     $("section.header .background").css({
//       "-webkit-transform":
//         "scale(" +
//         backgroundScale +
//         ") translateY(" +
//         translateBackground +
//         "px)",
//       transform:
//         "scale(" +
//         backgroundScale +
//         ") translateY(" +
//         translateBackground +
//         "px)",
//       opacity: 1 - scale / 1.2,
//     });

//     $body.removeClass("contentPart").removeClass("bottomPart");
//   } else if (scrollTop >= window.headerHeight) {
//     $body.addClass("contentPart").removeClass("bottomPart");
//   }

//   if (scrollProgress >= 0 && scrollProgress <= 1) {
//     $(".panel.top .progress")
//       .removeClass("hide")
//       .css("width", 100 * scrollProgress + "%");
//   } else if (scrollProgress > 1) {
//     $body.removeClass("contentPart");
//   } else if (scrollProgress < 0) {
//     $(".panel.top .progress").addClass("hide").css("width", "0%");
//   }
// });

$(document).ready(function () {
  if ($("#post__spy").css("display") !== "none") {
    $("body").scrollspy({ target: "#post__spy", offset: 90 });
  }
  
  if ($("#post__spy__mobile").css("display") !== "none") {
    $("body").scrollspy({ target: "#post__spy__mobile", offset: 90 });
  }

  
  $(".post-full-content img").wrap(function () {
    if (
      !$(this).parent().hasClass("swiper-slide") &&
      $(this).parent()[0].tagName != "A"
    ) {
      var temp = "<a class='fancybox-item' data-fancybox='images'";
      var href = "href='" + $(this).attr("src") + "'";
      var sub = "";
      if ($(this).attr("alt") != undefined && $(this).attr("alt") != "") {
        sub = " data-caption='" + $(this).attr("alt") + "'";
      }

      return temp + href + sub + " ></a>";
    }
  });

  // $().fancybox({
  //   selector : '.imglist a:visible',
  //   thumbs   : {
  //     autoStart : true
  //   }
  // });

  // $('#lightgallery-post').lightGallery({
  //     pager: false,
  //     share: false,
  //     loop: false,
  //     selector: '.lightgallery-item'
  // });
  

  $(".mgz__adv__close").on("click", function(e){
    $(".mgz__adv__scrollspy").addClass("d-none");
  })
});
