$.fn.hasHorizontalScrollBar = function() {
  return $(this).width() < this[0].scrollWidth;
};

function wrapScrollClassInit(wrap) {
  const $wrap = wrap;
  const $tar = $wrap.find('.wrap__scroll__target ');
  const $btn = $wrap.find('.wrap__scroll__next');

  if ($wrap.hasHorizontalScrollBar()) {
    $wrap.addClass('wrap-active');

    $wrap.each((index, element) => {
      const $element = $(element);

      const $section = $element.parents('section');
      $section.css('--wrap-bg-color', $section.css('background-color'));
    });

    $btn.on('click', () => {
      $tar.animate({ scrollLeft: $tar.scrollLeft() + 100 }, 'fast');
    });
  } else {
    $wrap.removeClass('wrap-active');
    $btn.unbind('click');
  }
}
function wrapScrollClass(wrap) {
  wrapScrollClassInit(wrap);
  window.addEventListener('resize', () => {
    wrapScrollClassInit(wrap);
  });
}
$(function() {
  const footerDichVu = $('.wrap__scroll__service__footer');
  // eslint-disable-next-line no-unused-expressions
  footerDichVu.length && wrapScrollClass(footerDichVu);
});
