

function autocompleteSearch(inp) {
  const searchQuickInput = document.querySelector(".search-quick-input");
  const searchQuickSuggestions = document.querySelector(".search-quick");

  const searchRecent = document.querySelector(".search-suggest");

  // const urlMomo = "/__post/Search/Suggest";
  const urlMomo = "http://www.mocky.io/v2/5ec7a32e2f00004b00427316";

  // const githubAjax = "https://api.github.com/search/repositories";

  const searchQuickIcon = [
    {
      name: "Keyword",
      icon: "search",
    },
    {
      name: "MomoPage",
      icon: "file-text",
    },
    {
      name: "Service",
      icon: "file-text",
    },
    {
      name: "PlaceBrand",
      icon: "file-text",
    },
    {
      name: "Promotion",
      icon: "file-text",
    },
    {
      name: "CashBackCategory",
      icon: "file-text",
    },
    {
      name: "Guide",
      icon: "help-circle",
    },
    {
      name: "QA",
      icon: "help-circle",
    },
    {
      name: "Article",
      icon: "file-text",
    },
    {
      name: "Blog",
      icon: "file-text",
    },
  ];

  var currentFocus;

  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = x.length - 1;
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("is-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("is-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
      except the one passed as an argument:*/
    var x = document.querySelectorAll(".search-quick-item");

    for (var i = 0; i < x.length; i++) {
      if (elmnt != searchQuickInput) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }

  function displayMatches(matchArray, txt) {
    const html = matchArray
      .map((item) => {
        const regex = new RegExp(txt, "gi");
        const cont = item.Name.replace(
          regex,
          `<strong class="">${txt}</strong>`
        );
        const link = item.UrlRewrite;
        const type = item.Type;
        let icon = "";
        searchQuickIcon.forEach(function (e) {
          if (e.name == type) {
            icon = e.icon;
          }
        });

        return `
              <li role="presentation" class="search-quick-item">
                  <a href="${link}" class="search-quick-link" target="_self" role="menuitem" tabindex="-1">
  
  
                      <span data-purpose="label"> ${cont} </span>
                  </a>
              </li>
          `;
      })
      .join("");
    searchQuickSuggestions.innerHTML = html;
  }

  var searchAjaxResult = debounce(function (e) {
    //   var txt = this.value.toLowerCase();
    var txt = this.value;

    //if (!txt) {
    //  txt = "circle";
    //}

    if (txt.length < 3 && txt) {
      closeAllLists();
      return;
    }

    currentFocus = -1;
    $.ajax({
      url: urlMomo,
      headers: {
        "X-Requested-With": "XMLHttpRequest",
        "Content-Type": "application/x-www-form-urlencoded",
      },
      method: "POST",
      dataType: "json",
      cache: !1,
      data: {
        q: txt,
        c: "3", //Total shit
      },
      success: function (data) {
        // Data lấy đc về
        let dataGet = [];

        for (var property1 in data) {
          dataGet = dataGet.concat(data[property1].Items);
        }
        displayMatches(dataGet, txt);
      },
      error: function (error) {
        console.log(error);
      },
    });
  }, 300);

  document.addEventListener("click", function (e) {
    closeAllLists(e.target);
  });

  searchQuickInput.addEventListener("keydown", function (e) {
    var x = document.querySelector(".search-quick");
    if (x) x = x.getElementsByClassName("search-quick-item");

    if (e.keyCode == 40) {
      e.preventDefault();

      currentFocus++;
      addActive(x);
    } else if (e.keyCode == 38) {
      e.preventDefault();

      currentFocus--;
      addActive(x);
    } else if (e.keyCode == 13) {
      if (currentFocus > -1) {
        e.preventDefault();
        if (x) {
          location.href = x[currentFocus]
            .querySelector(".search-quick-link")
            .getAttribute("href");
        }
      }
    }
  });
  searchQuickInput.addEventListener("input", searchAjaxResult);
  searchQuickInput.addEventListener("focus", searchAjaxResult);
}
autocompleteSearch();